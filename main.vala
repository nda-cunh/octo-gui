using Gtk;

void main(string[] args)
{
	Gtk.init(ref args);
	//Instance des objets BLOCK
	var window = new Gtk.Window();
	var boxPrincipal = new Gtk.Box(Gtk.Orientation.VERTICAL, 2);
	var boxLoader = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 2);
	var boxCfg = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 2);
	var boxGEFsync = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 2);

	//instance des lignes block
	var label_Loader = new Gtk.Label("Version de wine:");
	var comboBox_Loader = new Gtk.ComboBox();

	var button_File			= new Gtk.Button.with_label("File");	
	var button_Winecfg		= new Gtk.Button.with_label("Winecfg");	
	var button_Winetricks	= new Gtk.Button.with_label("Winetricks");	
	var button_Library		= new Gtk.Button.with_label("Library");
	
	// GEFSYNC
	var switch_esync		= new Gtk.Switch();
	var label_esync			= new Gtk.Label("ESync");
	var switch_fsync		= new Gtk.Switch();
	var label_fsync			= new Gtk.Label("FSync");
	var switch_gamemode		= new Gtk.Switch();
	var label_gamemode		= new Gtk.Label("Gamemode");

	//button-run
	var button_run = new Gtk.Button.with_label("Lancer");
	button_run.clicked.connect( () => {print("ca lance");});

	//Principal CONTAINER
	window.add(boxPrincipal);
	boxPrincipal.pack_start(new Gtk.Label ("Nom de l'application"), false, false, 0);
	boxPrincipal.pack_start(boxLoader, false, false, 0);
	boxPrincipal.pack_start(boxCfg, false, false, 0);
	boxPrincipal.pack_start(boxGEFsync, false, false, 0);
	boxPrincipal.pack_start(button_run, false, false, 0);

	//Loader CONTAINER
	boxLoader.pack_start(label_Loader, false, false, 0);
	boxLoader.pack_start(comboBox_Loader, false, false, 0);
	//CONFIG CONTAINER
	boxCfg.pack_start(button_File, true, true, 0);
	boxCfg.pack_start(button_Winecfg, true, true, 0);
	boxCfg.pack_start(button_Winetricks, true, true, 0);
	boxCfg.pack_start(button_Library, true, true, 0);
	//EFGSYNC CONTAINER

	boxGEFsync.pack_start(label_esync, false, false, 0);
	boxGEFsync.pack_start(switch_esync, false, false, 0);
	boxGEFsync.pack_start(label_fsync, false, false, 0);
	boxGEFsync.pack_start(switch_fsync, false, false, 0);
	boxGEFsync.pack_start(label_gamemode, false, false, 0);
	boxGEFsync.pack_start(switch_gamemode, false, false, 0);

	//Evénement
	window.destroy.connect(Gtk.main_quit);

	window.show_all();

	Gtk.main ();
}
